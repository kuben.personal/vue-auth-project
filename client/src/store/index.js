import { createStore } from "vuex";

export default createStore({
  state: {
    currentEmail: "",
    currentPassword: "",
    authenticated: true,
  },
  getters: {},
  mutations: {
    setEmailPassword(state, { email, password }) {
      state.currentEmail = email;
      state.currentPassword = password;
    },

    setAuth(state, { auth }) {
      state.authenticated = auth;
    },
  },
  actions: {},
  modules: {},
});
