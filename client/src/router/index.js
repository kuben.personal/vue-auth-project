import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import CreateProfile from "../views/CreateProfile.vue";
import MyProfile from "../views/MyProfile.vue";
import Users from "../views/Users.vue";

const routes = [
  { path: "/", component: Register },
  { path: "/login", component: Login },
  { path: "/create-profile", component: CreateProfile },
  { path: "/my-profile", component: MyProfile },
  { path: "/users", component: Users },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
