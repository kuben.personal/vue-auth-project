const router = require("express").Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { randomUUID } = require("crypto");
const mysql = require("mysql");

// TODO: put private key in env variable
PRIVATE_KEY = "my secret key";

var db;

function handleDisconnect() {
  db = mysql.createConnection({
    user: "bb3707e5747b61",
    host: "us-cdbr-east-05.cleardb.net",
    password: "d5fb0f29",
    database: "heroku_9fb6bb870b0c8f8",
  });

  db.connect(function (err) {
    if (err) {
      console.log("error when connecting to db:", err);
      setTimeout(handleDisconnect, 2000);
    }
  });
  db.on("error", function (err) {
    console.log("db error", err);
    if (err.code === "PROTOCOL_CONNECTION_LOST") {
      handleDisconnect();
    } else {
      throw err;
    }
  });
}

handleDisconnect();

router.post("/register", async (req, res) => {
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);
  const uuid = randomUUID();

  db.query(
    `INSERT INTO authuserdb (id, email, password, username, dob, phone, address)
    VALUES (
        '${uuid}',
        '${req.body.email}',
        '${hashedPassword}',
        '${req.body.username}',
        '${req.body.dob}',
        '${req.body.phone}',
        '${req.body.address}');`,
    (err, result) => {
      if (err) {
        res.send(err.code);
      } else {
        const token = jwt.sign({ _id: uuid }, PRIVATE_KEY);

        res.cookie("jwt", token, {
          httpOnly: true,
          maxAge: 24 * 60 * 60 * 1000, // 1 day
        });

        res.send({
          message: "success",
        });
      }
    }
  );
});

router.post("/check-email", async (req, res) => {
  db.query(
    `SELECT * from authuserdb WHERE email='${req.body.email}';`,
    (err, result) => {
      if (err) {
        res.status(500).send({
          message: err.code,
        });
      } else if (result.length > 0) {
        res.status(200).send({
          message: "email found",
        });
      } else {
        res.send({
          message: "email not found",
        });
      }
    }
  );
});

router.post("/login", async (req, res) => {
  function getUser() {
    return new Promise((resolve, reject) => {
      db.query(
        `SELECT * from authuserdb WHERE email='${req.body.email}';`,
        (err, result) => {
          if (err) {
            throw err;
          }
          resolve(result);
        }
      );
    });
  }

  let result;
  try {
    result = await getUser();
    if (result.length == 0) {
      return res.status(404).send({
        message: "user not found",
      });
    }
  } catch (err) {
    return res.status(500).send({
      message: err.code,
    });
  }
  const user = result["0"];
  if (!(await bcrypt.compare(req.body.password, user.password))) {
    return res.status(400).send({
      message: "invalid credentials",
    });
  }

  const token = jwt.sign({ _id: user.id }, PRIVATE_KEY);

  res.cookie("jwt", token, {
    httpOnly: true,
    maxAge: 24 * 60 * 60 * 1000, // 1 day
  });

  res.send({
    message: "success",
  });
});

router.get("/profile", async (req, res) => {
  try {
    const cookie = req.cookies["jwt"];

    const claims = jwt.verify(cookie, PRIVATE_KEY);

    if (!claims) {
      return res.status(401).send({
        message: "unauthenticated",
      });
    }

    db.query(
      `SELECT * from authuserdb WHERE id='${claims._id}';`,
      (err, result) => {
        if (err) {
          res.status(500).send({
            message: err.code,
          });
        }
        const { password, id, ...data } = result["0"];
        res.send(data);
      }
    );
  } catch (e) {
    return res.status(401).send({
      message: "unauthenticated",
    });
  }
});

router.post("/logout", (req, res) => {
  res.cookie("jwt", "", { maxAge: 0 });

  res.send({
    message: "success",
  });
});

router.get("/users", async (req, res) => {
  try {
    const cookie = req.cookies["jwt"];

    const claims = jwt.verify(cookie, PRIVATE_KEY);

    if (!claims) {
      return res.status(401).send({
        message: "unauthenticated",
      });
    }

    db.query(`SELECT * from authuserdb;`, (err, result) => {
      if (err) {
        res.status(500).send({
          message: err.code,
        });
      }

      let filteredResult = [];
      result.forEach(async (res) => {
        const { password, id, ...data } = res;
        filteredResult.push(data);
      });

      res.send(filteredResult);
    });
  } catch (e) {
    return res.status(401).send({
      message: "unauthenticated",
    });
  }
});

module.exports = router;
